import request from '@/utils/request'

// 查询单设计器保存demo列表
export function listDemo(query) {
  return request({
    url: '/designer/demo/list',
    method: 'get',
    params: query
  })
}

// 查询单设计器保存demo详细
export function getDemo(id) {
  return request({
    url: '/designer/demo/' + id,
    method: 'get'
  })
}

// 新增单设计器保存demo
export function addDemo(data) {
  return request({
    url: '/designer/demo',
    method: 'post',
    data: data
  })
}

// 修改单设计器保存demo
export function updateDemo(data) {
  return request({
    url: '/designer/demo',
    method: 'put',
    data: data
  })
}

// 删除单设计器保存demo
export function delDemo(id) {
  return request({
    url: '/designer/demo/' + id,
    method: 'delete'
  })
}
