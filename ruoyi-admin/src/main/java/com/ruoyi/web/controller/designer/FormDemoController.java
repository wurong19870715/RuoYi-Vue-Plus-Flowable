package com.ruoyi.web.controller.designer;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.designer.domain.vo.FormDemoVo;
import com.ruoyi.designer.domain.bo.FormDemoBo;
import com.ruoyi.designer.service.IFormDemoService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 单设计器保存demo
 *
 * @author ruoyi
 * @date 2023-08-03
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/designer/demo")
public class FormDemoController extends BaseController {

    private final IFormDemoService iFormDemoService;

    /**
     * 查询单设计器保存demo列表
     */
    @SaCheckPermission("system:demo:list")
    @GetMapping("/list")
    public TableDataInfo<FormDemoVo> list(FormDemoBo bo, PageQuery pageQuery) {
        return iFormDemoService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出单设计器保存demo列表
     */
    @SaCheckPermission("system:demo:export")
    @Log(title = "单设计器保存demo", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(FormDemoBo bo, HttpServletResponse response) {
        List<FormDemoVo> list = iFormDemoService.queryList(bo);
        ExcelUtil.exportExcel(list, "单设计器保存demo", FormDemoVo.class, response);
    }

    /**
     * 获取单设计器保存demo详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("system:demo:query")
    @GetMapping("/{id}")
    public R<FormDemoVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iFormDemoService.queryById(id));
    }

    /**
     * 新增单设计器保存demo
     */
    @SaCheckPermission("system:demo:add")
    @Log(title = "单设计器保存demo", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody FormDemoBo bo) {
        return toAjax(iFormDemoService.insertByBo(bo));
    }

    /**
     * 修改单设计器保存demo
     */
    @SaCheckPermission("system:demo:edit")
    @Log(title = "单设计器保存demo", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody FormDemoBo bo) {
        return toAjax(iFormDemoService.updateByBo(bo));
    }

    /**
     * 删除单设计器保存demo
     *
     * @param ids 主键串
     */
    @SaCheckPermission("system:demo:remove")
    @Log(title = "单设计器保存demo", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iFormDemoService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
