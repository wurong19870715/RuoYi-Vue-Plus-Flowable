package com.ruoyi.designer.mapper;

import com.ruoyi.designer.domain.FormDemo;
import com.ruoyi.designer.domain.vo.FormDemoVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 单设计器保存demoMapper接口
 *
 * @author ruoyi
 * @date 2023-08-03
 */
public interface FormDemoMapper extends BaseMapperPlus<FormDemoMapper, FormDemo, FormDemoVo> {

}
