package com.ruoyi.designer.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.designer.domain.bo.FormDemoBo;
import com.ruoyi.designer.domain.vo.FormDemoVo;
import com.ruoyi.designer.domain.FormDemo;
import com.ruoyi.designer.mapper.FormDemoMapper;
import com.ruoyi.designer.service.IFormDemoService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 单设计器保存demoService业务层处理
 *
 * @author ruoyi
 * @date 2023-08-03
 */
@RequiredArgsConstructor
@Service
public class FormDemoServiceImpl implements IFormDemoService {

    private final FormDemoMapper baseMapper;

    /**
     * 查询单设计器保存demo
     */
    @Override
    public FormDemoVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询单设计器保存demo列表
     */
    @Override
    public TableDataInfo<FormDemoVo> queryPageList(FormDemoBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<FormDemo> lqw = buildQueryWrapper(bo);
        Page<FormDemoVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询单设计器保存demo列表
     */
    @Override
    public List<FormDemoVo> queryList(FormDemoBo bo) {
        LambdaQueryWrapper<FormDemo> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<FormDemo> buildQueryWrapper(FormDemoBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<FormDemo> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), FormDemo::getName, bo.getName());
        lqw.eq(StringUtils.isNotBlank(bo.getJsonData()), FormDemo::getJsonData, bo.getJsonData());
        return lqw;
    }

    /**
     * 新增单设计器保存demo
     */
    @Override
    public Boolean insertByBo(FormDemoBo bo) {
        FormDemo add = BeanUtil.toBean(bo, FormDemo.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改单设计器保存demo
     */
    @Override
    public Boolean updateByBo(FormDemoBo bo) {
        FormDemo update = BeanUtil.toBean(bo, FormDemo.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(FormDemo entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除单设计器保存demo
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
