package com.ruoyi.designer.service;

import com.ruoyi.designer.domain.FormDemo;
import com.ruoyi.designer.domain.vo.FormDemoVo;
import com.ruoyi.designer.domain.bo.FormDemoBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 单设计器保存demoService接口
 *
 * @author ruoyi
 * @date 2023-08-03
 */
public interface IFormDemoService {

    /**
     * 查询单设计器保存demo
     */
    FormDemoVo queryById(Long id);

    /**
     * 查询单设计器保存demo列表
     */
    TableDataInfo<FormDemoVo> queryPageList(FormDemoBo bo, PageQuery pageQuery);

    /**
     * 查询单设计器保存demo列表
     */
    List<FormDemoVo> queryList(FormDemoBo bo);

    /**
     * 新增单设计器保存demo
     */
    Boolean insertByBo(FormDemoBo bo);

    /**
     * 修改单设计器保存demo
     */
    Boolean updateByBo(FormDemoBo bo);

    /**
     * 校验并批量删除单设计器保存demo信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
