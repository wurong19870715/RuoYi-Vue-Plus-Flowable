package com.ruoyi.designer.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 单设计器保存demo视图对象 form_demo
 *
 * @author ruoyi
 * @date 2023-08-03
 */
@Data
@ExcelIgnoreUnannotated
public class FormDemoVo {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 标题
     */
    @ExcelProperty(value = "标题")
    private String name;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String jsonData;


}
