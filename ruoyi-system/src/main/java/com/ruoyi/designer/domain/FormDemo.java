package com.ruoyi.designer.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 单设计器保存demo对象 form_demo
 *
 * @author ruoyi
 * @date 2023-08-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("form_demo")
public class FormDemo extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     *
     */
    private Long id;
    /**
     * 标题
     */
    private String name;
    /**
     *
     */
    private String jsonData;

}
