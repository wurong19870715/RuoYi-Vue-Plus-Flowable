package com.ruoyi.designer.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 单设计器保存demo业务对象 form_demo
 *
 * @author ruoyi
 * @date 2023-08-03
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class FormDemoBo extends BaseEntity {

    /**
     *
     */
    private Long id;

    /**
     * 标题
     */
    private String name;

    /**
     *
     */
    private String jsonData;


}
